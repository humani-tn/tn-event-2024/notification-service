package web

import (
	"encoding/json"
	"net/http"
	"notification-service/internal/config"
	"notification-service/internal/rbmq"
	"strings"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"github.com/rabbitmq/amqp091-go"
)

type Message struct {
	Op  string `json:"op"`
	Msg string `json:"msg"`
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func Setup(g *gin.Engine) {
	conf := config.GetConfig()

	if conf.Mode == "dev" {
		gin.SetMode(gin.DebugMode)
		upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	} else {
		gin.SetMode(gin.ReleaseMode)
	}

	// conf := config.GetConfig()
	g.GET("/", func(c *gin.Context) {
		ch := rbmq.GetChannel()

		q, err := ch.QueueDeclare(
			"",    // name
			false, // durable
			true,  // delete when unused
			false, // exclusive
			false, // no-wait
			nil,   // arguments
		)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"error": "couldn't create queue",
			})
			return
		}

		// Redirect all messages from the queue to the WebSocket connection
		msgs, err := ch.Consume(
			q.Name, // queue
			"",     // consumer
			true,   // auto-ack
			false,  // exclusive
			false,  // no-local
			false,  // no-wait
			nil,    // args
		)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"error": "couldn't consume queue",
			})
			return
		}

		handleWebSocket(c, q, msgs)
	})
}

func handleWebSocket(c *gin.Context, q amqp091.Queue, msgs <-chan amqp091.Delivery) {
	// Upgrade the HTTP connection to a WebSocket connection
	conn, err := upgrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	defer conn.Close()

	var mx sync.Mutex

	// Send messages from the queue to the WebSocket
	go func() {
		for msg := range msgs {
			mx.Lock()
			if err := conn.WriteMessage(websocket.TextMessage, msg.Body); err != nil {
				return
			}
			mx.Unlock()
		}
	}()

	// Handle WebSocket messages
	for {
		messageType, p, err := conn.ReadMessage()
		if err != nil {
			break
		}

		if messageType != websocket.TextMessage {
			continue
		}

		// Get the message from the WebSocket
		var msg Message
		if err := json.Unmarshal(p, &msg); err != nil {
			continue
		}

		var outMsg *Message

		switch msg.Op {
		case "sub":
			// Bind the queue to the exchange
			ch := rbmq.GetChannel()
			if err := ch.QueueBind(
				q.Name,     // queue name
				msg.Msg,    // routing key
				"messages", // exchange
				false,      // no-wait
				nil,        // arguments
			); err != nil {
				continue
			}

			outMsg = &Message{
				Op:  "success",
				Msg: "subscribed",
			}

			// When subscribing, we also send all the messages that were sent that match the routing key
			rbmq.CacheLock.RLock()
			for k, v := range rbmq.Cache {
				// Match with the same rules as the exchange
				if !rbmq.MatchTopic(k, msg.Msg) {
					continue
				}

				// Check that the message is not too old
				// pot top tombola live have no expiration
				if time.Since(v.Time) > 1*time.Minute && !strings.Contains(k, "pot") && !strings.Contains(k, "top") && !strings.Contains(k, "tombola") && !strings.Contains(k, "live") {
					continue
				}

				mx.Lock()
				if err := conn.WriteMessage(websocket.TextMessage, v.Raw); err != nil {
					return
				}
				mx.Unlock()
			}
			rbmq.CacheLock.RUnlock()
		case "unsub":
			// Unbind the queue from the exchange
			ch := rbmq.GetChannel()
			if err := ch.QueueUnbind(
				q.Name,     // queue name
				msg.Msg,    // routing key
				"messages", // exchange
				nil,        // arguments
			); err != nil {
				continue
			}

			outMsg = &Message{
				Op:  "success",
				Msg: "unsubscribed",
			}
		}

		if outMsg == nil {
			continue
		}

		mx.Lock()
		// Send the response to the WebSocket
		if err := conn.WriteJSON(outMsg); err != nil {
			break
		}
		mx.Unlock()
	}
	ch := rbmq.GetChannel()

	// Delete the queue when the WebSocket is closed
	ch.QueueDelete(q.Name, false, false, false)
}
