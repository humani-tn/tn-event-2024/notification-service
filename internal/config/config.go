package config

import (
	"fmt"

	"github.com/caarlos0/env/v9"
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
)

type Config struct {
	Port          string `env:"PORT" envDefault:"8080"`
	AdminUsername string `env:"ADMIN_USERNAME" envDefault:"admin"`
	AdminPassword string `env:"ADMIN_PASSWORD" envDefault:"admin"`
	GuestUsername string `env:"GUEST_USERNAME" envDefault:"guest"`
	GuestPassword string `env:"GUEST_PASSWORD" envDefault:"guest"`
	RbmqHost      string `env:"RBMQ_HOST" envDefault:"localhost"`
	RbmqPort      string `env:"RBMQ_PORT" envDefault:"5672"`
	LogLevel      string `env:"LOG_LEVEL" envDefault:"info"`
	Mode          string `env:"MODE" envDefault:"dev"`
}

var config Config

func GetConfig() Config {
	return config
}

func init() {
	godotenv.Load()
	if err := env.Parse(&config); err != nil {
		logrus.Fatal(err)
	}

	logrus.SetLevel(logrus.InfoLevel)
	if config.LogLevel == "debug" {
		logrus.SetLevel(logrus.DebugLevel)
	}

	logrus.Info("Loaded config: ", fmt.Sprintf("%+v", config))
}
