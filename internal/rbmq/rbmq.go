package rbmq

import (
	"fmt"
	"notification-service/internal/config"

	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/sirupsen/logrus"
)

var conn *amqp.Connection
var ch *amqp.Channel

func Setup() {
	cfg := config.GetConfig()
	var err error
	conn, err = amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s/public", cfg.AdminUsername, cfg.AdminPassword, cfg.RbmqHost, cfg.RbmqPort))
	if err != nil {
		logrus.Fatal(err)
	}

	ch, err = conn.Channel()
	if err != nil {
		logrus.Fatal(err)
	}

	SetupCache()
}

func GetChannel() *amqp.Channel {
	if ch == nil {
		Setup()
	}

	if ch.IsClosed() {
		logrus.Info("Channel is closed, reinitializing")
		Setup()
	}

	return ch
}
