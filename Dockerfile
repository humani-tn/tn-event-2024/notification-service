# Build go app
FROM golang AS builder

WORKDIR /app

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main cmd/server/main.go

# Build final image
FROM scratch

WORKDIR /app

COPY --from=builder /app/main .
# COPY --from=builder /app/www www

CMD ["./main"]
