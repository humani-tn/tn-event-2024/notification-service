package main

import (
	"notification-service/internal/config"
	"notification-service/internal/rbmq"
	"notification-service/web"

	"github.com/gin-gonic/gin"
)

func main() {
	c := config.GetConfig()

	config.InitRbmq()
	rbmq.Setup()

	g := gin.New()

	web.Setup(g)

	g.Run(":" + c.Port) // listen and serve on
}
