package config

import (
	"fmt"

	rabbithole "github.com/michaelklishin/rabbit-hole/v2"
	"github.com/sirupsen/logrus"
)

func InitRbmq() {
	// Make sure RabbitMQ has the correct configuration :
	rmqc, err := rabbithole.NewClient(fmt.Sprintf("http://%s:15672", config.RbmqHost), config.AdminUsername, config.AdminPassword)
	if err != nil {
		logrus.Fatal(err)
	}

	if err := rbmq_createUser(rmqc, config.GuestUsername, config.GuestPassword); err != nil {
		logrus.Fatal(err)
	}

	logrus.Info("RabbitMQ guest user created")

	if err := rbmq_createVhost(rmqc); err != nil {
		logrus.Fatal(err)
	}

	logrus.Info("RabbitMQ public vhost created")

	if err := rbmq_assignUserToVhost(rmqc, config.GuestUsername); err != nil {
		logrus.Fatal(err)
	}

	logrus.Info("RabbitMQ guest user assigned to public vhost")

	if err := rbmq_createExchange(rmqc, "messages"); err != nil {
		logrus.Fatal(err)
	}

	logrus.Info("RabbitMQ messages exchange created")
}

func rbmq_createUser(rmqc *rabbithole.Client, username, password string) error {
	users, err := rmqc.ListUsers()
	if err != nil {
		return err
	}

	for _, user := range users {
		if user.Name == username {
			return nil
		}
	}

	_, err = rmqc.PutUser(username, rabbithole.UserSettings{Password: password})
	if err != nil {
		return err
	}

	return nil
}

func rbmq_createVhost(rmqc *rabbithole.Client) error {
	vhosts, err := rmqc.ListVhosts()
	if err != nil {
		return err
	}

	for _, vhost := range vhosts {
		if vhost.Name == "public" {
			return nil
		}
	}

	_, err = rmqc.PutVhost("public", rabbithole.VhostSettings{})
	if err != nil {
		return err
	}

	return nil
}

func rbmq_assignUserToVhost(rmqc *rabbithole.Client, username string) error {
	_, err := rmqc.UpdatePermissionsIn("/", username, rabbithole.Permissions{
		Configure: "",
		Write:     "",
		Read:      "",
	})
	if err != nil {
		return err
	}

	_, err = rmqc.UpdatePermissionsIn("public", username, rabbithole.Permissions{
		Configure: "",
		Write:     "",
		Read:      ".*",
	})

	return err
}

func rbmq_createExchange(rmqc *rabbithole.Client, exchangeName string) error {
	exchanges, err := rmqc.ListExchanges()
	if err != nil {
		return err
	}

	for _, exchange := range exchanges {
		if exchange.Name == exchangeName {
			return nil
		}
	}

	_, err = rmqc.DeclareExchange("public", "messages", rabbithole.ExchangeSettings{
		Type:    "topic",
		Durable: true,
	})
	if err != nil {
		return err
	}

	return nil
}
