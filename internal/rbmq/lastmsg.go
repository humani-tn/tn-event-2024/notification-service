package rbmq

import (
	"regexp"
	"strings"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
)

type Message struct {
	Raw  []byte
	Time time.Time
}

var CacheLock sync.RWMutex
var Cache = make(map[string]*Message)

func SetupCache() {
	// Create a queue that receives all messages
	q, err := ch.QueueDeclare(
		"lastmsg", // name
		false,     // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	if err != nil {
		logrus.Fatal(err)
	}

	// Bind it to all messages
	err = ch.QueueBind(
		q.Name,     // queue name
		"#",        // routing key
		"messages", // exchange
		false,      // no-wait
		nil,        // arguments
	)
	if err != nil {
		logrus.Fatal(err)
	}

	go func() {
		msgs, err := ch.Consume(
			q.Name, // queue
			"",     // consumer
			true,   // auto-ack
			true,   // exclusive
			false,  // no-local
			false,  // no-wait
			nil,    // args
		)
		if err != nil {
			logrus.Fatal(err)
		}

		for msg := range msgs {
			CacheLock.Lock()
			Cache[msg.RoutingKey] = &Message{
				Raw:  msg.Body,
				Time: time.Now(),
			}
			CacheLock.Unlock()
		}
	}()
}

func MatchTopic(routingKey, pattern string) bool {
	// Convert RabbitMQ topic pattern to a regular expression
	pattern = "^" + regexp.QuoteMeta(pattern) + "$"
	pattern = strings.ReplaceAll(pattern, `\#`, "[^.]+")
	pattern = strings.ReplaceAll(pattern, `\*`, "[^.]+")

	r := regexp.MustCompile(pattern)

	return r.MatchString(routingKey)
}
